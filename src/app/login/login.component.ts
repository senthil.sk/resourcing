import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators, FormControl} from '@angular/forms';
import {AuthenticateService} from '../services/authentication.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loggedin: any;
  email = null;

  constructor(
    private router: Router,
    private authService: AuthenticateService,
    private formBuilder: FormBuilder
  ) {
    const userDetails = sessionStorage.getItem('userDetails');
    if (userDetails !== undefined && userDetails !== null) {
      this.router.navigate(['/']);
    }
  }

  logForm: FormGroup;
  errorMessage = null;
  loggedIn = false;
  showHomeIcon = true;

  validationMessages = {
    email: [
      {type: 'required', message: 'Email is required.'},
    ],
    password: [
      {type: 'required', message: 'Password is required.'},
      {type: 'minlength', message: 'Password must be at least 5 characters long.'}
    ]
  };

  ngOnInit() {
    this.logForm = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
    });
  }


  loginUser(value) {
    value.email = 'senthilk979@gmail.com';
    value.password = '9578563099';
    this.authService.loginUser(value)
      .then(res => {
        if (res.user.emailVerified) {
          sessionStorage.setItem('userId', res.user.uid);
          sessionStorage.setItem('email', res.user.email);
          sessionStorage.setItem('verified', res.user.emailVerified);
          sessionStorage.setItem('userDetails', JSON.stringify(res.user));
          this.loggedIn = true;
          this.errorMessage = '';
          // this.getLoginUserDetails(res.user.email);
          this.router.navigate(['/home']);
        } else {
          this.errorMessage = 'Dear user, please verify your email address by clicking on the link we sent to your email account.';
        }
      }, err => {
        if (err.code === 'auth/user-not-found') {
          this.errorMessage = 'User not found with the provided email id. Please check your email id.';
        } else if (err.code === 'auth/wrong-password') {
          this.errorMessage = 'Please verify your password.';
        } else {
          this.errorMessage = err.message;
        }
      });
  }

  resetPassword(email) {
    if (email !== null) {
      this.authService.resetPassword(email).then(() => {
        // success
      }).catch((error) => {
        if (error.code === 'auth/user-not-found') {
          this.errorMessage = 'User not found with the provided email id. Please check your email id.';
        } else {
          this.errorMessage = error.message;
        }
        return false;
      });
      return false;
    } else {
      this.errorMessage = 'Please enter valid email to reset.';
      return false;
    }
  }

  // getLoginUserDetails(email) {
  //   this.authService.getLoggedInUser(email).snapshotChanges().subscribe(res => {
  //     res.forEach(item => {
  //       const a: any = item.payload.toJSON();
  //       // tslint:disable-next-line: no-string-literal
  //       a['$key'] = item.key;
  //       sessionStorage.setItem('appUser', a);
  //     });
  //   });
  // }

}
