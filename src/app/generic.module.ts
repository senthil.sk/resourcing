import {NgModule} from '@angular/core';
import {SortDirective} from './directive/sort.directive';

@NgModule({
  imports: [],
  exports: [SortDirective],
  declarations: [SortDirective]
})
export class GenericModule {
}
