import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {AddAssociateComponent} from './add-associate.component';
import {FileService} from '../services/file.services';
import {AssociateComponent} from './associate.component';
import {QuillModule} from 'ngx-quill';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {GenericModule} from '../generic.module';
import {NgbDateParserFormatter, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ViewAssociateComponent} from './view-associate.component';
import {MomentDateFormatter} from '../directive/momentdateformatter';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: 'add-associate',
        component: AddAssociateComponent
      },
      {
        path: 'edit-associate/:id',
        component: AddAssociateComponent
      },
      {
        path: '',
        component: AssociateComponent
      },
      {
        path: 'view-associate/:id',
        component: ViewAssociateComponent
      },
    ]),
    QuillModule,
    InfiniteScrollModule,
    GenericModule,
    NgbModule
  ],
  providers: [
    FileService,
    {provide: NgbDateParserFormatter, useClass: MomentDateFormatter}
  ],
  declarations: [AddAssociateComponent, AssociateComponent, ViewAssociateComponent]
})
export class AssociateModule {
}
