import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {GenericService} from '../services/generic.service';
import {HttpClient} from '@angular/common/http';
import * as _ from 'lodash';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';

declare var $: any;

@Component({
  selector: 'app-associate',
  templateUrl: './associate.component.html'
})
export class AssociateComponent implements OnInit {

  associates = [];
  // @ts-ignore
  associates$: Observable<any[]> = [];
  associateValue: any;
  searchOn = false;
  searchQueryList = {};
  defaultValues = {};
  modules = {
    toolbar: [
      ['bold', 'italic', 'underline'],
      ['blockquote'],
      [{header: 1}, {header: 2}],
      [{list: 'bullet'}],
      [{color: []}],
      [{align: []}],
      ['link', 'image', 'video']
    ]
  };
  mailContent = '';
  toList = 'r.raghavendran@tcs.com, t.senthilkumar4@tcs.com, senthil.thangavel@bnpparibasfortis.com';
  subject = '';
  successMsg = null;
  errorMessage = null;
  lastResponse = null;
  filterAssociatesForm: FormGroup;
  filterMe = new FormControl('');

  constructor(
    private router: Router,
    private genericService: GenericService,
    private http: HttpClient,
    private formBuilder: FormBuilder) {
    $('#' + $('#main-nav li.active')[0].id).removeClass('active');
    $('#associates-li').addClass('active');
    this.associates$ = this.filterMe.valueChanges.pipe(
      startWith(''),
      map(text => this.search(text))
    );
  }

  ngOnInit(): void {
    this.loadTable();
    this.genericService.getListOfObjects('defaultValues').valueChanges().subscribe((res: any) => {
      if (!_.isEmpty(res)) {
        this.defaultValues = res[0];
      }
    });
  }

  loadTable() {
    this.searchOn = false;
    this.associates = [];
    this.initializeFilterForm();
    this.genericService.getListOfObjectsWithLimit('associates').snapshotChanges().subscribe((res) => {
      if (res !== null && res !== undefined && res.length > 0) {
        this.associates = [];
        res.forEach(item => {
          const a: any = item.payload.doc.data();
          a.key = item.payload.doc.id;
          a.date = new Date(a.modifiedDate);
          this.lastResponse = item.payload.doc;
          this.associates.push(a);
        });
        this.associates = _.uniqBy(this.associates, 'key');
        this.associates = _.orderBy(this.associates, ['date'], ['desc']);
        this.filterMe.setValue(' ');
        this.filterMe.setValue('');
      }
    });
  }

  loadNextPage() {
    if (this.searchOn) {
      // tslint:disable-next-line:max-line-length
      this.genericService.getListOfObjectsForSearchQuery('associates', this.searchQueryList, this.lastResponse).snapshotChanges().subscribe((res) => {
        if (res !== null && res !== undefined && res.length > 0) {
          res.forEach(item => {
            const a: any = item.payload.doc.data();
            a.key = item.payload.doc.id;
            a.date = new Date(a.modifiedDate);
            this.lastResponse = item.payload.doc;
            this.associates.push(a);
          });
          this.associates = _.uniqBy(this.associates, 'key');
          this.associates = _.orderBy(this.associates, ['date'], ['desc']);
        }
      });
    } else {
      this.genericService.getListOfObjectsAfterLastResponse('associates', this.lastResponse).snapshotChanges().subscribe((res) => {
        if (res !== null && res !== undefined && res.length > 0) {
          res.forEach(item => {
            const a: any = item.payload.doc.data();
            a.key = item.payload.doc.id;
            a.date = new Date(a.modifiedDate);
            this.lastResponse = item.payload.doc;
            this.associates.push(a);
          });
          this.associates = _.uniqBy(this.associates, 'key');
          this.associates = _.orderBy(this.associates, ['date'], ['desc']);
        }
      });
    }
  }

  search(text: string): any[] {
    if (text === '') {
      return this.associates;
    }
    return this.associates.filter(associate => {
      const term = text.toLowerCase();
      return associate.name.toLowerCase().includes(term)
        || associate.skillSet.toLowerCase().includes(term)
        || associate.currentCompany.toLowerCase().includes(term)
        || associate.email.toLowerCase().includes(term)
        || associate.referredBy.toLowerCase().includes(term)
        || associate.status.toLowerCase().includes(term)
        || associate.contactNumber.toLowerCase().includes(term)
        || associate.experience.toLowerCase().includes(term);
    });
  }

  sendMail() {
    const data = {
      toMail: '',
      html: this.mailContent,
      subject: this.subject,
      fileName: this.associateValue.name + '_' + this.associateValue.skillSet + '_' + 'resume.pdf',
      resume: this.associateValue.resume
    };
    if (!_.isEmpty(this.toList)) {
      const toList = this.toList.split(',');
      this.successMsg = null;
      this.errorMessage = null;
      for (const toMail of toList) {
        if (/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(toMail.trim())) {
          data.toMail = toMail.trim();
          setTimeout(() => {
            this.http.post('api/sendmail', data).subscribe((response) => {
              this.successMsg = 'Mail Sent Successfully.';
            }, (error => {
              this.errorMessage = 'Sorry, there is some technical issue while sending mail. Please try again.';
            }));
          }, 1000);
        }
      }
      if (_.isEmpty(this.errorMessage)) {
        $('#mailModal').modal('hide');
      }
    }
  }

  composeMail(associate) {
    this.associateValue = associate;
    // tslint:disable-next-line:max-line-length
    let defaultContent = '<p><span style="color: rgb(0, 41, 102)">Dear,</span></p><p></p><p><span style="color: rgb(0, 41, 102)">Please find the details of <b>' +
      associate.name + '</b>.';
    if (!_.isEmpty(associate.resume)) {
      defaultContent = defaultContent + ' Attached resume as well.';
    }
    defaultContent = defaultContent + '</span></p><p><br></p>';
    let experience = associate.experience;
    if (!_.isEmpty(associate.experience) && experience.toLowerCase().indexOf('years') < 0) {
      experience = experience + ' years';
    }
    // tslint:disable-next-line:max-line-length
    let basicDetails = '<p><span style="color: rgb(0, 41, 102)"><b>Name: </b>' + associate.name + '</span></p><p><span style="color: rgb(0, 41, 102)"><b>Experience: </b> ' + experience + '</span></p><p><span style="color: rgb(0, 41, 102)"><b>Skill set: </b> ' + associate.skillSet + '</span></p><p><span style="color: rgb(0, 41, 102)"><b>Email: </b> ' + associate.email + '</span></p><p><span style="color: rgb(0, 41, 102)"><b>Contact Number: </b> ' + associate.contactNumber + '</span></p>';
    if (!_.isEmpty(associate.alternateContactNumber)) {
      basicDetails = '<p><span style="color: rgb(0, 41, 102)"><b>Alternate Number: </b> ' + associate.alternateContactNumber + '</span></p>';
    }
    // tslint:disable-next-line:max-line-length
    let companyDetails = '<p><span style="color: rgb(0, 41, 102)"><b>Current Company: </b>' + associate.currentCompany + '</span></p><p><span style="color: rgb(0, 41, 102)"><b>Notice Period: </b>' + associate.noticePeriod + '</span></p><p><span style="color: rgb(0, 41, 102)"><b>Referral Method: </b>' + associate.referralMethod + '</span></p>';
    if (!_.isEmpty(associate.EPNumber)) {
      companyDetails = '<p><span style="color: rgb(0, 41, 102)"><b>EP Number: </b>' + associate.EPNumber + '</span></p>';
    }
    if (!_.isEmpty(associate.EPNumber)) {
      companyDetails = '<p><span style="color: rgb(0, 41, 102)"><b>EP Number: </b>' + associate.EPNumber + '</span></p>';
    }
    if (!_.isEmpty(associate.lastDate)) {
      companyDetails = '<p><span style="color: rgb(0, 41, 102)"><b>Last Date: </b>' + associate.lastDate + '</span></p>';
    }
    const data = '';
    // tslint:disable-next-line:max-line-length
    const sign = '<p><span style="color: rgb(0, 41, 102);">Regards,</span><br/><strong style="color: rgb(0, 41, 102);">Senthil Kumar</strong><br/><strong style="color: rgb(0, 41, 102);">senthilk979@gmail.com, senthil.thangavel@bnpparibasfortis.com</strong></p><p><br></p></div>';
    this.mailContent = data + defaultContent + basicDetails + companyDetails + '<p><br></p>' + sign;
    this.subject = associate.name + ' - ' + associate.skillSet + ' - ' + experience + ' - ' + associate.referralMethod;
  }

  deleteAssociate() {
    this.genericService.deleteObject('associates', this.associateValue.key);
  }

  checkYears(exp) {
    return exp.toLowerCase().indexOf('years') < 0;
  }

  filterAssociates(searchQuery) {
    this.searchOn = true;
    this.searchQueryList = searchQuery;
    this.associates = [];
    $('#filterModal').modal('hide');
    this.genericService.getListOfObjectsForSearchQuery('associates', searchQuery, null).snapshotChanges().subscribe((res) => {
      if (res !== null && res !== undefined && res.length > 0) {
        this.associates = [];
        res.forEach(item => {
          const a: any = item.payload.doc.data();
          // tslint:disable-next-line: no-string-literal
          a.key = item.payload.doc.id;
          a.date = new Date(a.modifiedDate);
          this.lastResponse = item.payload.doc;
          this.associates.push(a);
        });
        this.associates = _.uniqBy(this.associates, 'key');
        this.associates = _.orderBy(this.associates, ['date'], ['desc']);
        // this.lastResponse = this.associates[this.associates.length - 1].key;
        console.log(this.associates);
      }
    });
  }

  onScrollUp() {
    console.log('do nothing');
  }

  initializeFilterForm() {
    this.filterAssociatesForm = this.formBuilder.group({
      name: new FormControl('', []),
      skillSet: new FormControl('', []),
      experience: new FormControl('', []),
      currentCompany: new FormControl('', []),
      noticePeriod: new FormControl('', []),
      referralMethod: new FormControl('', []),
      referredBy: new FormControl('', []),
      status: new FormControl('', []),
    });
  }

}
