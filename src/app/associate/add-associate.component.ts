import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import * as _ from 'lodash';
import {FileService, FileUpload} from '../services/file.services';
import {GenericService} from '../services/generic.service';
declare var $: any;

@Component({
  selector: 'app-add-associate',
  templateUrl: './add-associate.component.html'
})
export class AddAssociateComponent implements OnInit {


  addAssociateForm: FormGroup;
  errorMessage: string = null;
  email = null;
  file = null;
  editId = null;
  editMode = false;
  statusList = [];
  designationList = [];
  locations = [];
  expertiseLevels = [];
  skillSets = [];
  grades = [];
  releaseReasons = [];
  sources = [];
  group = 1;
  accounts = [];
  visaTypes = [];
  visaStatusList = [];
  customerRoles = [];
  squads = [];
  clusters = [];
  tribes = [];

  validationMessages = {};

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private fileService: FileService,
              private genericService: GenericService,
              private route: ActivatedRoute) {
    this.email = sessionStorage.getItem('email');
    $('#' + $('#main-nav li.active')[0].id).removeClass('active');
    $('#associates-li').addClass('active');
  }

  ngOnInit() {
    this.editId = this.route.snapshot.params.id;
    this.initiateForm();
    this.setValidationMessages();
    this.loadDropdownValues();
    if (!_.isEmpty(this.editId)) {
      this.editMode = true;
      this.genericService.getObject('associates', this.editId).valueChanges().subscribe((res) => {
        this.addAssociateForm.patchValue(res);
      }, error => {
        this.errorMessage = 'There is some in retrieving the details of the associate. Please try again later.';
        setTimeout(() => {
          this.goBack();
        }, 1000);
      });
    }
  }

  initiateForm() {
    this.addAssociateForm = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      name: new FormControl('', Validators.compose([
        Validators.required
      ])),
      contactNumber: new FormControl('', Validators.compose([
        Validators.required
      ])),
      alternateContactNumber: new FormControl(''),
      experience: new FormControl('', Validators.compose([
        Validators.required
      ])),
      currentCompany: new FormControl(''),
      noticePeriod: new FormControl('', Validators.compose([
        Validators.required
      ])),
      skillSet: new FormControl('', Validators.compose([
        Validators.required
      ])),
      lastDate: new FormControl(''),
      resumeFile: new FormControl(''),
      resume: new FormControl(''),
      referralMethod: new FormControl('', Validators.compose([
        Validators.required
      ])),
      EPNumber: new FormControl(''),
      referredBy: new FormControl('', Validators.compose([Validators.required])),
      status: new FormControl('')
    });
  }

  setValidationMessages() {
    this.validationMessages = {
      email: [
        {type: 'required', message: 'Email is required.'},
        {type: 'pattern', message: 'Enter a valid email.'}
      ],
      name: [
        {type: 'required', message: 'Password is required.'},
        {type: 'minlength', message: 'Password must be at least 5 characters long.'}
      ],
      currentCompany: [
        {type: 'required', message: 'Current Company is required.'}
      ],
      referralMethod: [
        {type: 'required', message: 'Referral method is required.'}
      ],
      noticePeriod: [
        {type: 'required', message: 'Notice period is required.'}
      ],
      skillSet: [
        {type: 'required', message: 'Skill set is required.'}
      ],
      contactNumber: [
        {type: 'required', message: 'Contact Number is required.'}
      ],
      experience: [
        {type: 'required', message: 'Experience is required.'}
      ]
    };
  }

  fileChangeEvent(fileInput: any) {
    this.errorMessage = null;
    if (fileInput.target.files && fileInput.target.files[0]) {
      // Size Filter Bytes
      const maxSize = 20971520;
      const allowedTypes = ['image/png', 'image/jpeg', 'application/msword',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'image/jpg',
        'image/jpeg',
        'application/pdf',
        'image/png',
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];

      if (fileInput.target.files[0].size > maxSize) {
        this.errorMessage =
          'Maximum size allowed is ' + maxSize / 1000 + 'Mb';
        return false;
      }

      if (!_.includes(allowedTypes, fileInput.target.files[0].type)) {
        this.errorMessage = 'Only these file types are allowed ( JPG | PNG | PDF | Excel | Word )';
        return false;
      }
      this.file = fileInput.target.files[0];
    }
  }

  saveAssociate(associate) {
    if (this.addAssociateForm.valid) {
      const uDetails = sessionStorage.getItem('email');
      if (_.isEmpty(this.email) && _.isEmpty(uDetails)) {
        this.router.navigate(['/login']);
      } else if (_.isEmpty(this.email) && !_.isEmpty(uDetails)) {
        this.email = uDetails;
      }
      associate.modifiedBy = this.email;
      associate.modifiedByUserId = sessionStorage.getItem('userId');
      associate.modifiedDate = new Date();
      associate.resumeFile = null;
      if (this.file !== null && this.file !== undefined) {
        const fileValue = new FileUpload(this.file);
        this.fileService.pushFileToStorage(fileValue, 'associates', associate, 'resume', this.editId, true);
      } else if (this.editMode) {
        this.genericService.updateObject('associates', this.editId, associate);
        this.goBack();
      } else {
        this.genericService.addToList('associates', associate);
        this.goBack();
      }
    } else {
      this.errorMessage = 'Please fill all the mandatory fields and try saving the form.';
    }
  }

  goBack() {
    window.history.back();
  }

  loadDropdownValues() {
    this.genericService.getListOfObjects('designations').valueChanges().subscribe((res: any) => {
      if (!_.isEmpty(res)) {
        this.designationList = res;
      } else {
        this.goBack();
      }
    });
    this.genericService.getListOfObjects('employmentGrades').valueChanges().subscribe((res: any) => {
      if (!_.isEmpty(res)) {
        this.grades = res;
      } else {
        this.goBack();
      }
    });
    this.genericService.getListOfObjects('skillsets').valueChanges().subscribe((res: any) => {
      if (!_.isEmpty(res)) {
        this.skillSets = res;
      } else {
        this.goBack();
      }
    });
    this.genericService.getListOfObjects('expertiseLevels').valueChanges().subscribe((res: any) => {
      if (!_.isEmpty(res)) {
        this.expertiseLevels = res;
      } else {
        this.goBack();
      }
    });
    this.genericService.getListOfObjects('employeeLocations').valueChanges().subscribe((res: any) => {
      if (!_.isEmpty(res)) {
        this.locations = res;
      } else {
        this.goBack();
      }
    });
    this.genericService.getListOfObjects('customerProfileStatus').valueChanges().subscribe((res: any) => {
      if (!_.isEmpty(res)) {
        this.statusList = res;
      } else {
        this.goBack();
      }
    });
    this.genericService.getListOfObjects('accounts').valueChanges().subscribe((res: any) => {
      if (!_.isEmpty(res)) {
        this.accounts = res;
      } else {
        this.goBack();
      }
    });
    this.genericService.getListOfObjects('profileSources').valueChanges().subscribe((res: any) => {
      if (!_.isEmpty(res)) {
        this.sources = res;
      } else {
        this.goBack();
      }
    });
    this.genericService.getListOfObjects('releaseReasons').valueChanges().subscribe((res: any) => {
      if (!_.isEmpty(res)) {
        this.releaseReasons = res;
      } else {
        this.goBack();
      }
    });
    this.genericService.getListOfObjects('visaStatus').valueChanges().subscribe((res: any) => {
      if (!_.isEmpty(res)) {
        this.visaStatusList = res;
      } else {
        this.goBack();
      }
    });
    this.genericService.getListOfObjects('customerRoles').valueChanges().subscribe((res: any) => {
      if (!_.isEmpty(res)) {
        this.customerRoles = res;
      } else {
        this.goBack();
      }
    });
    this.genericService.getListOfObjects('tribes').valueChanges().subscribe((res: any) => {
      if (!_.isEmpty(res)) {
        this.tribes = res;
      } else {
        this.goBack();
      }
    });
    this.genericService.getListOfObjects('clusters').valueChanges().subscribe((res: any) => {
      if (!_.isEmpty(res)) {
        this.clusters = res;
      } else {
        this.goBack();
      }
    });
    this.genericService.getListOfObjects('squads').valueChanges().subscribe((res: any) => {
      if (!_.isEmpty(res)) {
        this.squads = res;
      } else {
        this.goBack();
      }
    });
  }
}
