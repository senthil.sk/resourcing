import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import * as _ from 'lodash';
import {FileService, FileUpload} from '../services/file.services';
import {GenericService} from '../services/generic.service';
declare var $: any;

@Component({
  selector: 'app-view-associate',
  templateUrl: './view-associate.component.html'
})
export class ViewAssociateComponent implements OnInit {


  addInitialTRForm: FormGroup;
  addVRForm: FormGroup;
  errorMessage: string = null;
  email = null;
  associateId = null;
  associate = null;
  editId = null;
  editMode = false;
  statusList = [];
  currentView = 1;
  modules = {
    toolbar: [
      ['bold', 'italic', 'underline'],
      [{header: 1}, {header: 2}],
      [{list: 'bullet'}],
      [{color: []}],
      [{align: []}]
    ]
  };

  validationMessages = {};

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private fileService: FileService,
              private genericService: GenericService,
              private route: ActivatedRoute) {
    this.email = sessionStorage.getItem('email');
    $('#' + $('#main-nav li.active')[0].id).removeClass('active');
    $('#associates-li').addClass('active');
    this.initializeForms();
  }

  ngOnInit() {
    this.associateId = this.route.snapshot.params.id;
    this.genericService.getObject('associates', this.associateId).valueChanges().subscribe((res) => {
      this.associate = res;
      this.addInitialTRForm.patchValue({dateOfDiscussion: new Date()});
    }, error => {
      this.errorMessage = 'There is some in retrieving the details of the associate. Please try again later.';
      setTimeout(() => {
        this.goBack();
      }, 1000);
    });

  }

  saveITRForm() {

  }

  saveVRForm() {

  }

  goBack() {
    window.history.back();
  }

  initializeForms() {
    this.addInitialTRForm = this.formBuilder.group({
      name: new FormControl('', Validators.compose([
        Validators.required
      ])),
      dateOfDiscussion: new FormControl(new Date()),
      feedback: new FormControl(''),
    });
    this.addVRForm = this.formBuilder.group({
      tr1: new FormControl('', Validators.compose([
        Validators.required
      ])),
      tr2: new FormControl(''),
      mr1: new FormControl('', Validators.compose([
        Validators.required
      ])),
      mr2: new FormControl(''),
      hr1: new FormControl(''),
      hr2: new FormControl(''),
      dateOfDiscussion: new FormControl(new Date()),
      timeOfDiscussion: new FormControl({hour: 0, minute: 0}),
      feedback: new FormControl(''),
    });
  }

}
