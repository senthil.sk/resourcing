import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import * as _ from 'lodash';
import {FileService, FileUpload} from '../services/file.services';
import {GenericService} from '../services/generic.service';

declare var $: any;

@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.component.html',
  styleUrls: ['./view-profile.component.scss']
})
export class ViewProfileComponent implements OnInit {


  addInitialTRForm: FormGroup;
  addVRForm: FormGroup;
  errorMessage: string = null;
  email = null;
  profileId = null;
  profile = null;
  editId = null;
  editMode = false;
  technicalReviewers = [];
  managementReviewers = [];
  initialTechnicalReviews = [];
  videoRounds = [];
  currentView = 1;
  modules = {
    toolbar: [
      ['bold', 'italic', 'underline'],
      [{header: 1}, {header: 2}],
      [{list: 'bullet'}],
      [{color: []}],
      [{align: []}]
    ]
  };

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private fileService: FileService,
              private genericService: GenericService,
              private route: ActivatedRoute) {
    this.email = sessionStorage.getItem('email');
    $('#' + $('#main-nav li.active')[0].id).removeClass('active');
    $('#profile-li').addClass('active');
    this.initializeForms();
  }

  ngOnInit() {
    this.profileId = this.route.snapshot.params.id;
    this.genericService.getObject('profiles', this.profileId).valueChanges().subscribe((res) => {
      this.profile = res;
    }, error => {
      this.errorMessage = 'There is some in retrieving the details of the profile. Please try again later.';
      setTimeout(() => {
        this.goBack();
      }, 1000);
    });

  }

  saveITRForm() {
    if (this.addInitialTRForm.valid) {
      const data = this.addInitialTRForm.value;
      data.modifiedBy = this.email;
      data.modifiedByUserId = sessionStorage.getItem('userId');
      data.modifiedDate = new Date();
      data.profileId = this.profileId;
      if (this.editMode) {
        this.genericService.updateObject('initialTechnicalReviews', this.editId, data);
        $('#addInitialTRModal').modal('hide');
      } else {
        this.genericService.addToList('initialTechnicalReviews', data);
        $('#addInitialTRModal').modal('hide');
      }
    }
  }

  saveVRForm() {
    if (this.addVRForm.valid) {
      const data = this.addVRForm.value;
      data.modifiedBy = this.email;
      data.modifiedByUserId = sessionStorage.getItem('userId');
      data.modifiedDate = new Date();
      data.profileId = this.profileId;
      if (this.editMode) {
        this.genericService.updateObject('videoRounds', this.editId, data);
        $('#addVRModal').modal('hide');
      } else {
        this.genericService.addToList('videoRounds', data);
        $('#addVRModal').modal('hide');
      }
    }
  }

  goBack() {
    window.history.back();
  }

  initializeForms() {
    this.addInitialTRForm = this.formBuilder.group({
      name: new FormControl('', Validators.compose([
        Validators.required
      ])),
      dateOfDiscussion: new FormControl(''),
      feedback: new FormControl(''),
      decision: new FormControl(''),
      profileId: this.profileId
    });
    this.addVRForm = this.formBuilder.group({
      client: new FormControl(''),
      finalDecision: new FormControl(''),
      dateOfDiscussion: new FormControl(''),
      timeOfDiscussion: new FormControl(''),
      feedback: new FormControl(''),
      profileId: this.profileId
    });
  }

  getReviewerDetails() {
    if (this.managementReviewers.length === 0) {
      this.genericService.getListOfObjects('managementReviewers').valueChanges().subscribe((res) => {
        if (!_.isEmpty(res)) {
          res.forEach(item => {
            // @ts-ignore
            this.managementReviewers.push(item.value);
          });
          this.managementReviewers = _.uniqBy(this.managementReviewers, 'employeeId');
        } else {
          this.goBack();
        }
      });
      this.genericService.getListOfObjects('technicalReviewers').valueChanges().subscribe((res) => {
        if (!_.isEmpty(res)) {
          res.forEach(item => {
            // @ts-ignore
            this.technicalReviewers.push(item.value);
          });
          this.technicalReviewers = _.uniqBy(this.technicalReviewers, 'employeeId');
        } else {
          this.goBack();
        }
      });
      setTimeout(() => {
        this.getVRDetails();
      }, 500);
    }
  }

  checkIsVRDone(status) {
    const statusList = ['Initial TR Pending', 'Initial TR Completed', 'Initial TR Rejected'];
    return statusList.indexOf(status) < 0;
  }

  getVRDetails() {
    this.genericService.getListOfObjectsByKey('videoRounds', this.profileId).snapshotChanges().subscribe((res) => {
      if (res !== null && res.length > 0) {
        res.forEach(item => {
          const a: any = item.payload.doc.data();
          a.key = item.payload.doc.id;
          // const tr1Details = _.find(this.technicalReviewers, {employeeId: a.tr1});
          // const tr2Details = _.find(this.technicalReviewers, {employeeId: a.tr2});
          // const mr1Details = _.find(this.managementReviewers, {employeeId: a.mr1});
          // // const mr2Details = _.find(this.managementReviewers, {employeeId: a.mr2});
          // tr1Details.type = 'TR1#';
          // tr2Details.type = 'TR2#';
          // mr1Details.type = 'MR';
          // a.date = a.dateOfDiscussion.day + '-' + a.dateOfDiscussion.month + '-' + a.dateOfDiscussion.year;
          // // mr2Details.type = 'MR2#';
          // a.reviewerDetails = [];
          // a.reviewerDetails.push(tr1Details);
          // a.reviewerDetails.push(tr2Details);
          // a.reviewerDetails.push(mr1Details);
          // // a.reviewerDetails.push(mr2Details);
          a.date = a.dateOfDiscussion.day + ' - ' + a.dateOfDiscussion.month + ' - ' + a.dateOfDiscussion.year;
          this.videoRounds.push(a);
        });
        this.videoRounds = _.uniqBy(this.videoRounds, 'key');
        this.videoRounds = _.reverse(this.videoRounds);
      }
    });
  }

  getITRDetails() {
    this.genericService.getListOfObjectsByKey('initialTechnicalReviews', this.profileId).snapshotChanges().subscribe((res) => {
      if (res !== null && res.length > 0) {
        res.forEach(item => {
          const a: any = item.payload.doc.data();
          a.key = item.payload.doc.id;
          a.date = a.dateOfDiscussion.day + ' - ' + a.dateOfDiscussion.month + ' - ' + a.dateOfDiscussion.year;
          this.initialTechnicalReviews.push(a);
        });
        this.initialTechnicalReviews = _.uniqBy(this.initialTechnicalReviews, 'key');
        this.initialTechnicalReviews = _.reverse(this.initialTechnicalReviews);
      }
    });
  }

  editITR(itr: any) {

  }

  deleteITR(itr: any) {
    this.genericService.deleteObject('initialTechnicalReviews', itr.key);
    this.initialTechnicalReviews.splice(this.initialTechnicalReviews.indexOf(itr), 1);
  }

  editVR(vr: any) {

  }

  deleteVR(vr: any) {
    this.genericService.deleteObject('videoRounds', vr.key);
    this.videoRounds.splice(this.videoRounds.indexOf(vr), 1);
  }
}
