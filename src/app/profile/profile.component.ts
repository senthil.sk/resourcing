import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {GenericService} from '../services/generic.service';
import {HttpClient} from '@angular/common/http';
import * as _ from 'lodash';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';

declare var $: any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit {

  profiles = [];
  // @ts-ignore
  profiles$: Observable<any[]> = [];
  profileValue: any;
  searchOn = false;
  searchQueryList = {};
  defaultValues = {};
  modules = {
    toolbar: [
      ['bold', 'italic', 'underline'],
      ['blockquote'],
      [{header: 1}, {header: 2}],
      [{list: 'bullet'}],
      [{color: []}],
      [{align: []}],
      ['link', 'image', 'video']
    ]
  };
  mailContent = '';
  toList = 'r.raghavendran@tcs.com, t.senthilkumar4@tcs.com, senthil.thangavel@bnpparibasfortis.com';
  subject = '';
  successMsg = null;
  errorMessage = null;
  lastResponse = null;
  filterProfilesForm: FormGroup;
  filterMe = new FormControl('');

  constructor(
    private router: Router,
    private genericService: GenericService,
    private http: HttpClient,
    private formBuilder: FormBuilder) {
    $('#' + $('#main-nav li.active')[0].id).removeClass('active');
    $('#profile-li').addClass('active');
    this.profiles$ = this.filterMe.valueChanges.pipe(
      startWith(''),
      map(text => this.search(text))
    );
  }

  ngOnInit(): void {
    this.loadTable();
    this.genericService.getListOfObjects('defaultValues').valueChanges().subscribe((res: any) => {
      if (!_.isEmpty(res)) {
        this.defaultValues = res[0];
      }
    });
  }

  loadTable() {
    this.searchOn = false;
    this.profiles = [];
    this.initializeFilterForm();
    this.genericService.getListOfObjectsWithLimit('profiles').snapshotChanges().subscribe((res) => {
      if (res !== null && res !== undefined && res.length > 0) {
        this.profiles = [];
        res.forEach(item => {
          const a: any = item.payload.doc.data();
          a.key = item.payload.doc.id;
          a.date = new Date(a.modifiedDate);
          this.lastResponse = item.payload.doc;
          this.profiles.push(a);
        });
        this.profiles = _.uniqBy(this.profiles, 'key');
        this.profiles = _.orderBy(this.profiles, ['date'], ['desc']);
        this.filterMe.setValue(' ');
        this.filterMe.setValue('');
      }
    });
  }

  loadNextPage() {
    if (this.searchOn) {
      // tslint:disable-next-line:max-line-length
      this.genericService.getListOfObjectsForSearchQuery('profiles', this.searchQueryList, this.lastResponse).snapshotChanges().subscribe((res) => {
        if (res !== null && res !== undefined && res.length > 0) {
          res.forEach(item => {
            const a: any = item.payload.doc.data();
            a.key = item.payload.doc.id;
            a.date = new Date(a.modifiedDate);
            this.lastResponse = item.payload.doc;
            this.profiles.push(a);
          });
          this.profiles = _.uniqBy(this.profiles, 'key');
          this.profiles = _.orderBy(this.profiles, ['date'], ['desc']);
        }
      });
    } else {
      this.genericService.getListOfObjectsAfterLastResponse('profiles', this.lastResponse).snapshotChanges().subscribe((res) => {
        if (res !== null && res !== undefined && res.length > 0) {
          res.forEach(item => {
            const a: any = item.payload.doc.data();
            a.key = item.payload.doc.id;
            a.date = new Date(a.modifiedDate);
            this.lastResponse = item.payload.doc;
            this.profiles.push(a);
          });
          this.profiles = _.uniqBy(this.profiles, 'key');
          this.profiles = _.orderBy(this.profiles, ['date'], ['desc']);
        }
      });
    }
  }

  search(text: string): any[] {
    if (text === '') {
      return this.profiles;
    }
    return this.profiles.filter(profile => {
      const term = text.toLowerCase();
      return profile.name.toLowerCase().includes(term)
        || profile.skillSet.toLowerCase().includes(term)
        || profile.currentCompany.toLowerCase().includes(term)
        || profile.email.toLowerCase().includes(term)
        || profile.referredBy.toLowerCase().includes(term)
        || profile.status.toLowerCase().includes(term)
        || profile.contactNumber.toLowerCase().includes(term)
        || profile.experience.toLowerCase().includes(term);
    });
  }

  sendMail() {
    const data = {
      toMail: '',
      html: this.mailContent,
      subject: this.subject,
      fileName: this.profileValue.name + '_' + this.profileValue.skillSet + '_' + 'resume.pdf',
      resume: this.profileValue.resume
    };
    if (!_.isEmpty(this.toList)) {
      const toList = this.toList.split(',');
      this.successMsg = null;
      this.errorMessage = null;
      for (const toMail of toList) {
        if (/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(toMail.trim())) {
          data.toMail = toMail.trim();
          setTimeout(() => {
            this.http.post('api/sendmail', data).subscribe((response) => {
              this.successMsg = 'Mail Sent Successfully.';
            }, (error => {
              this.errorMessage = 'Sorry, there is some technical issue while sending mail. Please try again.';
            }));
          }, 1000);
        }
      }
      if (_.isEmpty(this.errorMessage)) {
        $('#mailModal').modal('hide');
      }
    }
  }

  composeMail(profile) {
    this.profileValue = profile;
    // tslint:disable-next-line:max-line-length
    let defaultContent = '<p><span style="color: rgb(0, 41, 102)">Dear,</span></p><p></p><p><span style="color: rgb(0, 41, 102)">Please find the details of <b>' +
      profile.name + '</b>.';
    if (!_.isEmpty(profile.resume)) {
      defaultContent = defaultContent + ' Attached resume as well.';
    }
    defaultContent = defaultContent + '</span></p><p><br></p>';
    let experience = profile.experience;
    if (!_.isEmpty(profile.experience) && experience.toLowerCase().indexOf('years') < 0) {
      experience = experience + ' years';
    }
    // tslint:disable-next-line:max-line-length
    let basicDetails = '<p><span style="color: rgb(0, 41, 102)"><b>Name: </b>' + profile.name + '</span></p><p><span style="color: rgb(0, 41, 102)"><b>Experience: </b> ' + experience + '</span></p><p><span style="color: rgb(0, 41, 102)"><b>Skill set: </b> ' + profile.skillSet + '</span></p><p><span style="color: rgb(0, 41, 102)"><b>Email: </b> ' + profile.email + '</span></p><p><span style="color: rgb(0, 41, 102)"><b>Contact Number: </b> ' + profile.contactNumber + '</span></p>';
    if (!_.isEmpty(profile.alternateContactNumber)) {
      basicDetails = '<p><span style="color: rgb(0, 41, 102)"><b>Alternate Number: </b> ' + profile.alternateContactNumber + '</span></p>';
    }
    // tslint:disable-next-line:max-line-length
    let companyDetails = '<p><span style="color: rgb(0, 41, 102)"><b>Current Company: </b>' + profile.currentCompany + '</span></p><p><span style="color: rgb(0, 41, 102)"><b>Notice Period: </b>' + profile.noticePeriod + '</span></p><p><span style="color: rgb(0, 41, 102)"><b>Referral Method: </b>' + profile.referralMethod + '</span></p>';
    if (!_.isEmpty(profile.EPNumber)) {
      companyDetails = '<p><span style="color: rgb(0, 41, 102)"><b>EP Number: </b>' + profile.EPNumber + '</span></p>';
    }
    if (!_.isEmpty(profile.EPNumber)) {
      companyDetails = '<p><span style="color: rgb(0, 41, 102)"><b>EP Number: </b>' + profile.EPNumber + '</span></p>';
    }
    if (!_.isEmpty(profile.lastDate)) {
      companyDetails = '<p><span style="color: rgb(0, 41, 102)"><b>Last Date: </b>' + profile.lastDate + '</span></p>';
    }
    const data = '';
    // tslint:disable-next-line:max-line-length
    const sign = '<p><span style="color: rgb(0, 41, 102);">Regards,</span><br/><strong style="color: rgb(0, 41, 102);">Senthil Kumar</strong><br/><strong style="color: rgb(0, 41, 102);">senthilk979@gmail.com, senthil.thangavel@bnpparibasfortis.com</strong></p><p><br></p></div>';
    this.mailContent = data + defaultContent + basicDetails + companyDetails + '<p><br></p>' + sign;
    this.subject = profile.name + ' - ' + profile.skillSet + ' - ' + experience + ' - ' + profile.referralMethod;
  }

  deleteProfile() {
    this.genericService.deleteObject('profiles', this.profileValue.key);
  }

  checkYears(exp) {
    return exp.toLowerCase().indexOf('years') < 0;
  }

  filterProfiles(searchQuery) {
    this.searchOn = true;
    this.searchQueryList = searchQuery;
    this.profiles = [];
    $('#filterModal').modal('hide');
    this.genericService.getListOfObjectsForSearchQuery('profiles', searchQuery, null).snapshotChanges().subscribe((res) => {
      if (res !== null && res !== undefined && res.length > 0) {
        this.profiles = [];
        res.forEach(item => {
          const a: any = item.payload.doc.data();
          // tslint:disable-next-line: no-string-literal
          a.key = item.payload.doc.id;
          a.date = new Date(a.modifiedDate);
          this.lastResponse = item.payload.doc;
          this.profiles.push(a);
        });
        this.profiles = _.uniqBy(this.profiles, 'key');
        this.profiles = _.orderBy(this.profiles, ['date'], ['desc']);
        // this.lastResponse = this.profiles[this.profiles.length - 1].key;
        console.log(this.profiles);
      }
    });
  }

  onScrollUp() {
    console.log('do nothing');
  }

  initializeFilterForm() {
    this.filterProfilesForm = this.formBuilder.group({
      name: new FormControl('', []),
      skillSet: new FormControl('', []),
      experience: new FormControl('', []),
      currentCompany: new FormControl('', []),
      noticePeriod: new FormControl('', []),
      referralMethod: new FormControl('', []),
      referredBy: new FormControl('', []),
      status: new FormControl('', []),
    });
  }

}
