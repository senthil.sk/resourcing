import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import * as _ from 'lodash';
import {FileService, FileUpload} from '../services/file.services';
import {GenericService} from '../services/generic.service';
import * as XLSX from 'xlsx';

declare var $: any;

@Component({
  selector: 'app-add-profile',
  templateUrl: './add-profile.component.html'
})
export class AddProfileComponent implements OnInit {


  addProfileForm: FormGroup;
  errorMessage: string = null;
  email = null;
  file = null;
  editId = null;
  editMode = false;
  statusList = [];

  validationMessages = {};

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private fileService: FileService,
              private genericService: GenericService,
              private route: ActivatedRoute) {
    this.email = sessionStorage.getItem('email');
    $('#' + $('#main-nav li.active')[0].id).removeClass('active');
    $('#profile-li').addClass('active');
  }

  ngOnInit() {
    this.editId = this.route.snapshot.params.id;
    this.initiateForm();
    this.setValidationMessages();
    this.genericService.getListOfObjects('tcsProfileStatus').valueChanges().subscribe((res: any) => {
      if (!_.isEmpty(res)) {
        this.statusList = res;
        if (this.editId === undefined || this.editId === null || this.editId === '') {
          this.addProfileForm.patchValue({status: this.statusList[this.statusList.length - 1].value});
          this.addProfileForm.patchValue({referredBy: this.email});
        }
        this.addCompany(null);
      } else {
        this.goBack();
      }
    });
    if (!_.isEmpty(this.editId)) {
      this.editMode = true;
      this.genericService.getObject('profiles', this.editId).valueChanges().subscribe((res) => {
        this.addProfileForm.patchValue(res);
      }, error => {
        this.errorMessage = 'There is some in retrieving the details of the profile. Please try again later.';
        setTimeout(() => {
          this.goBack();
        }, 1000);
      });
    }
  }

  initiateForm() {
    this.addProfileForm = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      name: new FormControl('', Validators.compose([
        Validators.required
      ])),
      contactNumber: new FormControl('', Validators.compose([
        Validators.required
      ])),
      alternateContactNumber: new FormControl(''),
      experience: new FormControl('', Validators.compose([
        Validators.required
      ])),
      currentCompany: new FormControl(''),
      noticePeriod: new FormControl(''),
      skillSet: new FormControl('', Validators.compose([
        Validators.required
      ])),
      lastDate: new FormControl(''),
      resumeFile: new FormControl(''),
      resume: new FormControl(''),
      referralMethod: new FormControl(''),
      EPNumber: new FormControl(''),
      referredBy: new FormControl(''),
      status: new FormControl('')
    });
  }

  setValidationMessages() {
    this.validationMessages = {
      email: [
        {type: 'required', message: 'Email is required.'},
        {type: 'pattern', message: 'Enter a valid email.'}
      ],
      name: [
        {type: 'required', message: 'Password is required.'},
        {type: 'minlength', message: 'Password must be at least 5 characters long.'}
      ],
      currentCompany: [
        {type: 'required', message: 'Current Company is required.'}
      ],
      referralMethod: [
        {type: 'required', message: 'Referral method is required.'}
      ],
      noticePeriod: [
        {type: 'required', message: 'Notice period is required.'}
      ],
      skillSet: [
        {type: 'required', message: 'Skill set is required.'}
      ],
      contactNumber: [
        {type: 'required', message: 'Contact Number is required.'}
      ],
      experience: [
        {type: 'required', message: 'Experience is required.'}
      ]
    };
  }

  fileChangeEvent(fileInput: any) {
    this.errorMessage = null;
    if (fileInput.target.files && fileInput.target.files[0]) {
      // Size Filter Bytes
      const maxSize = 40971520;
      const allowedTypes = ['image/png', 'image/jpeg', 'application/msword',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'image/jpg',
        'image/jpeg',
        'application/pdf',
        'image/png',
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];

      if (fileInput.target.files[0].size > maxSize) {
        this.errorMessage =
          'Maximum size allowed is ' + maxSize / 1000 + 'Mb';
        return false;
      }

      // if (!_.includes(allowedTypes, fileInput.target.files[0].type)) {
      //   this.errorMessage = 'Only these file types are allowed ( JPG | PNG | PDF | Excel | Word )';
      //   return false;
      // }
      this.file = fileInput.target.files[0];
    }
  }

  saveProfile(profile) {
    if (this.addProfileForm.valid) {
      const uDetails = sessionStorage.getItem('email');
      if (_.isEmpty(this.email) && _.isEmpty(uDetails)) {
        this.router.navigate(['/login']);
      } else if (_.isEmpty(this.email) && !_.isEmpty(uDetails)) {
        this.email = uDetails;
      }
      profile.modifiedBy = this.email;
      profile.modifiedByUserId = sessionStorage.getItem('userId');
      profile.modifiedDate = new Date();
      profile.resumeFile = null;
      this.addCompany(profile.currentCompany);
      if (this.file !== null && this.file !== undefined) {
        const fileValue = new FileUpload(this.file);
        this.fileService.pushFileToStorage(fileValue, 'profiles', profile, 'resume', this.editId, true);
      } else if (this.editMode) {
        this.genericService.updateObject('profiles', this.editId, profile);
        this.goBack();
      } else {
        this.genericService.addToList('profiles', profile);
        this.goBack();
      }
    } else {
      this.errorMessage = 'Please fill all the mandatory fields and try saving the form.';
    }
  }

  goBack() {
    window.history.back();
  }

  addCompany(company) {
    this.genericService.getListOfObjects('defaultValues').snapshotChanges().subscribe((res) => {
      if (res !== null && res.length > 0) {
        const defValues = [];
        let key;
        res.forEach(item => {
          const a: any = item.payload.doc.data();
          key = item.payload.doc.id;
          defValues.push(a);
        });
        const doc = defValues[0];
        if (!_.isEmpty(company) && doc.companiesList.indexOf(company) < 0) {
          doc.companiesList.push(company);
          this.genericService.updateObject('defaultValues', key, doc);
        }
      }
    });
  }

  addfile(event) {
    this.file = event.target.files[0];
    const fileReader = new FileReader();
    fileReader.readAsArrayBuffer(this.file);
    fileReader.onload = (e) => {
      const arrayBuffer = fileReader.result;
      // @ts-ignore
      const data = new Uint8Array(arrayBuffer);
      const arr = new Array();
      for (let i = 0; i !== data.length; ++i) {
        arr[i] = String.fromCharCode(data[i]);
      }
      const bstr = arr.join('');
      const workbook = XLSX.read(bstr, {type: 'binary'});
      console.log(workbook.SheetNames.length);
      for (const sheet of workbook.SheetNames) {
        console.log(sheet);
        const wk = workbook.Sheets[sheet];
        console.log(XLSX.utils.sheet_to_json(wk, {raw: true}));
      }
    };
  }
}

