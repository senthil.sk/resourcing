import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {AddProfileComponent} from './add-profile.component';
import {FileService} from '../services/file.services';
import {ProfileComponent} from './profile.component';
import {QuillModule} from 'ngx-quill';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {GenericModule} from '../generic.module';
import {NgbDateParserFormatter, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ViewProfileComponent} from './view-profile.component';
import {MomentDateFormatter} from '../directive/momentdateformatter';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: 'add-profile',
        component: AddProfileComponent
      },
      {
        path: 'edit-profile/:id',
        component: AddProfileComponent
      },
      {
        path: '',
        component: ProfileComponent
      },
      {
        path: 'view-profile/:id',
        component: ViewProfileComponent
      },
    ]),
    QuillModule,
    InfiniteScrollModule,
    GenericModule,
    NgbModule
  ],
  providers: [
    FileService,
    {provide: NgbDateParserFormatter, useClass: MomentDateFormatter}
  ],
  declarations: [AddProfileComponent, ProfileComponent, ViewProfileComponent]
})
export class ProfileModule {
}
