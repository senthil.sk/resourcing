import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {RequirementsComponent} from './requirements.component';
import {GenericModule} from '../generic.module';
import {FileService} from '../services/file.services';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: RequirementsComponent
      },
    ]),
    GenericModule,
    NgbModule
  ],
  providers: [FileService],
  declarations: [RequirementsComponent]
})
export class RequirementsModule {
}
