import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {GenericService} from '../services/generic.service';
import {HttpClient} from '@angular/common/http';
import * as _ from 'lodash';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {FileService, FileUpload} from '../services/file.services';

declare var $: any;

@Component({
  selector: 'app-requirements',
  templateUrl: './requirements.component.html'
})
export class RequirementsComponent implements OnInit {
  requirements = [];
  defaultValues = {};
  path = 'requirements';
  successMsg = null;
  errorMessage = null;
  deleteContent = null;
  addForm: FormGroup;
  email = null;
  editId = null;
  editMode = false;
  oldContent = null;
  file = null;
  requirement = {};

  constructor(private router: Router,
              private genericService: GenericService,
              private http: HttpClient,
              private formBuilder: FormBuilder,
              private fileService: FileService) {
    $('#' + $('#main-nav li.active')[0].id).removeClass('active');
    $('#requirements-li').addClass('active');
  }

  ngOnInit(): void {
    this.email = sessionStorage.getItem('email');
    // this.loadData();
    this.requirements = [{
      jobId: 'SRQ104492',
      tribe: 'Cards & Accounts',
      role: 'Project Manager Business 2',
      langReqd: 'Required',
      startDate: new Date(),
      count: 1,
      status: 'Won',
      closedOn: new Date(),
      closedWith: 'Siddharth',
      hitRatio: '100%'
    }];
    this.requirement = this.requirements[0];
    this.initializeForm();
  }

  loadData() {
    this.initializeForm();
    this.requirements = [];
    this.editMode = false;
    this.editId = null;
    this.genericService.getListOfObjects('requirements').snapshotChanges().subscribe((res) => {
      if (res !== null && res !== undefined && res.length > 0) {
        this.requirements = [];
        res.forEach(item => {
          const a: any = item.payload.doc.data();
          a.key = item.payload.doc.id;
          a.date = new Date(a.modifiedDate);
          this.requirements.push(a);
        });
        this.requirements = _.uniqBy(this.requirements, 'key');
        this.requirements = _.orderBy(this.requirements, ['date'], ['desc']);
        console.log(this.requirements);
      }
    });
  }

  deleteComponent() {
    this.genericService.deleteObject('requirements', this.deleteContent.key);
    this.requirements.splice(this.requirements.indexOf(this.deleteContent), 1);
    this.deleteContent = null;
  }

  initializeForm() {
    this.addForm = this.formBuilder.group({
      jobId: new FormControl('', Validators.compose([
        Validators.required
      ])),
      tribe: new FormControl('', Validators.compose([
        Validators.required
      ])),
      role: new FormControl('', Validators.compose([
        Validators.required
      ])),
      langReqd: new FormControl('', Validators.compose([
        Validators.required
      ])),
      startDate: new FormControl('', Validators.compose([
        Validators.required
      ])),
      count: new FormControl('1', Validators.compose([
        Validators.required
      ])),
      status: new FormControl('', Validators.compose([
        Validators.required
      ])),
      closedOn: new FormControl(''),
      closedWith: new FormControl(''),
      hitRatio: new FormControl('')
    });
  }

  saveForm() {
    if (this.addForm.valid && this.errorMessage === null) {
      const uDetails = sessionStorage.getItem('email');
      if (_.isEmpty(this.email) && _.isEmpty(uDetails)) {
        this.router.navigate(['/login']);
      } else if (_.isEmpty(this.email) && !_.isEmpty(uDetails)) {
        this.email = uDetails;
      }
      const data = {
        value: this.addForm.value.data,
        image: null,
        modifiedDate: new Date().toString(),
        modifiedBy: sessionStorage.getItem('email'),
        modifiedByUserId: sessionStorage.getItem('userId')
      };
      if (this.file !== null && this.file !== undefined) {
        const fileValue = new FileUpload(this.file);
        this.fileService.pushFileToStorage(fileValue, this.path, data, 'image', this.editId, false);
      } else if (this.editMode) {
        if (this.addForm.value.pic !== '' && this.addForm.value.pic !== null && this.addForm.value.pic !== undefined) {
          data.image = this.addForm.value.pic;
        }
        this.genericService.updateObject(this.path, this.editId, data);
      } else {
        this.genericService.addToList(this.path, data);
      }
      $('#addComponent').modal('hide');
      this.initializeForm();
    } else {
      this.errorMessage = 'Please fill all the mandatory fields and try saving the form.';
    }
  }

  openAddModal() {
    setTimeout(() => {
      document.getElementById('jobId').focus();
    }, 100);
  }

  clearAll() {
    this.editId = null;
    this.editMode = false;
    this.oldContent = null;
    this.initializeForm();
  }

  editComponent(component) {
    this.oldContent = component;
    this.editId = component.key;
    this.editMode = true;
    this.addForm.patchValue(component);
    this.openAddModal();
  }

  fileChangeEvent(fileInput: any) {
    this.errorMessage = null;
    if (fileInput.target.files && fileInput.target.files[0]) {
      // Size Filter Bytes
      const maxSize = 10971520;
      const allowedTypes = ['image/png', 'image/jpeg', 'image/jpg'];

      if (fileInput.target.files[0].size > maxSize) {
        this.errorMessage =
          'Maximum size allowed is ' + maxSize / 1000 + 'Mb';
        return false;
      }

      if (!_.includes(allowedTypes, fileInput.target.files[0].type)) {
        this.errorMessage = 'Only these file types are allowed ( JPG | PNG  )';
        return false;
      }
      this.file = fileInput.target.files[0];
    }
  }

  removeImage() {
    this.oldContent.image = null;
    this.addForm.patchValue({pic: ''});
  }


}
