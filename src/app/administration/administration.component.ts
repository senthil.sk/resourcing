import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {GenericService} from '../services/generic.service';
import {HttpClient} from '@angular/common/http';
import * as _ from 'lodash';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {FileService, FileUpload} from '../services/file.services';

declare var $: any;

@Component({
  selector: 'app-administration',
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.scss']
})
export class AdministrationComponent implements OnInit {
  response = [];
  path = 'accounts';
  defaultValues = {};
  successMsg = null;
  errorMessage = null;
  deleteContent = null;
  addForm: FormGroup;
  addReviewerForm: FormGroup;
  email = null;
  editId = null;
  editMode = false;
  oldContent = null;
  file = null;
  administrationComponents = [
    {
      name: 'Accounts',
      id: 'accounts'
    },
    {
      name: 'Delivery Managers',
      id: 'deliveryManagers'
    },
    {
      name: 'Clusters',
      id: 'clusters'
    },
    {
      name: 'Tribes',
      id: 'tribes'
    },
    {
      name: 'Squads',
      id: 'squads'
    },
    {
      name: 'Technical Reviewers',
      id: 'technicalReviewers'
    },
    {
      name: 'Management Reviewers',
      id: 'managementReviewers'
    },
    {
      name: 'Users',
      id: 'users'
    },
    {
      name: 'Designations',
      id: 'designations'
    },
    {
      name: 'Employment Grades',
      id: 'employmentGrades'
    },
    {
      name: 'Experience Groups',
      id: 'experienceGroups'
    },
    {
      name: 'Expertise Levels',
      id: 'expertiseLevels'
    },
    {
      name: 'Skill sets',
      id: 'skillsets'
    },
    {
      name: 'Customer Roles',
      id: 'customerRoles'
    },
    {
      name: 'Visa Status',
      id: 'visaStatus'
    },
    {
      name: 'Employee Locations',
      id: 'employeeLocations'
    },
    {
      name: 'Visa types',
      id: 'visaTypes'
    },
    {
      name: 'Customer Profile Status',
      id: 'customerProfileStatus'
    },
    {
      name: 'EP Profile Status',
      id: 'tcsProfileStatus'
    },
    {
      name: 'Employment Types',
      id: 'employmentTypes'
    },
    {
      name: 'Customer Request Types',
      id: 'customerRequestTypes'
    },
    {
      name: 'Profile Sources',
      id: 'profileSources'
    },
    {
      name: 'Profile Results',
      id: 'profileResults'
    },
    {
      name: 'Release Reasons',
      id: 'releaseReasons'
    },
  ];

  constructor(private router: Router,
              private genericService: GenericService,
              private http: HttpClient,
              private formBuilder: FormBuilder,
              private fileService: FileService) {
    $('#' + $('#main-nav li.active')[0].id).removeClass('active');
    $('#admin-li').addClass('active');
  }

  ngOnInit(): void {
    this.loadData(this.path);
    this.email = sessionStorage.getItem('email');
    this.genericService.getListOfObjects('defaultValues').valueChanges().subscribe((res: any) => {
      if (!_.isEmpty(res)) {
        this.defaultValues = res[0];
      }
    });
  }

  loadData(path) {
    this.initializeForm();
    this.path = path;
    this.response = [];
    this.editMode = false;
    this.editId = null;
    this.genericService.getListOfObjects(path).snapshotChanges().subscribe((res) => {
      if (res !== null && res !== undefined && res.length > 0) {
        this.response = [];
        res.forEach(item => {
          const a: any = item.payload.doc.data();
          // tslint:disable-next-line: no-string-literal
          a.key = item.payload.doc.id;
          a.date = new Date(a.modifiedDate);
          this.response.push(a);
        });
        this.response = _.uniqBy(this.response, 'key');
        this.response = _.orderBy(this.response, ['date'], ['desc']);
        console.log(this.response);
      }
    });
  }

  deleteComponent() {
    this.genericService.deleteObject(this.path, this.deleteContent.key);
    this.response.splice(this.response.indexOf(this.deleteContent), 1);
    this.deleteContent = null;
  }

  initializeForm() {
    this.addForm = this.formBuilder.group({
      data: new FormControl('', Validators.compose([
        Validators.required
      ])),
      pic: new FormControl('')
    });
    this.addReviewerForm = this.formBuilder.group({
      employeeId: new FormControl('', Validators.compose([
        Validators.required
      ])),
      name: new FormControl('', Validators.compose([
        Validators.required
      ])),
      grade: new FormControl('', Validators.compose([
        Validators.required
      ])),
      contact: new FormControl('', Validators.compose([
        Validators.required
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required
      ]))
    });
  }

  saveForm(input) {
    if ((this.addForm.valid || this.addReviewerForm.valid) && this.errorMessage === null) {
      const uDetails = sessionStorage.getItem('email');
      if (_.isEmpty(this.email) && _.isEmpty(uDetails)) {
        this.router.navigate(['/login']);
      } else if (_.isEmpty(this.email) && !_.isEmpty(uDetails)) {
        this.email = uDetails;
      }
      const data = {
        value: input,
        image: null,
        modifiedDate: new Date().toString(),
        modifiedBy: sessionStorage.getItem('email'),
        modifiedByUserId: sessionStorage.getItem('userId')
      };
      if (this.file !== null && this.file !== undefined) {
        const fileValue = new FileUpload(this.file);
        this.fileService.pushFileToStorage(fileValue, this.path, data, 'image', this.editId, false);
      } else if (this.editMode) {
        if (this.addForm.value.pic !== '' && this.addForm.value.pic !== null && this.addForm.value.pic !== undefined) {
          data.image = this.addForm.value.pic;
        }
        this.genericService.updateObject(this.path, this.editId, data);
      } else {
        this.genericService.addToList(this.path, data);
      }
      $('#addComponent').modal('hide');
      this.initializeForm();
    } else {
      this.errorMessage = 'Please fill all the mandatory fields and try saving the form.';
    }
  }

  getName(path) {
    return _.find(this.administrationComponents, {id: path});
  }

  openAddModal() {
    setTimeout(() => {
      document.getElementById('data').focus();
    }, 100);
  }

  clearAll() {
    this.editId = null;
    this.editMode = false;
    this.oldContent = null;
    this.initializeForm();
  }

  editComponent(component) {
    this.oldContent = component;
    this.editId = component.key;
    this.editMode = true;
    if (this.path === 'managementReviewers' || this.path === 'technicalReviewers') {
      this.addReviewerForm.patchValue(component.value);
    } else {
      this.addForm.patchValue({data: component.value});
      if (component.image !== null && component.image !== undefined && component.image !== '') {
        this.addForm.patchValue({pic: component.image});
      } else {
        this.addForm.patchValue({pic: ''});
      }
    }
    this.openAddModal();
  }

  fileChangeEvent(fileInput: any) {
    this.errorMessage = null;
    if (fileInput.target.files && fileInput.target.files[0]) {
      // Size Filter Bytes
      const maxSize = 10971520;
      const allowedTypes = ['image/png', 'image/jpeg', 'image/jpg'];

      if (fileInput.target.files[0].size > maxSize) {
        this.errorMessage =
          'Maximum size allowed is ' + maxSize / 1000 + 'Mb';
        return false;
      }

      if (!_.includes(allowedTypes, fileInput.target.files[0].type)) {
        this.errorMessage = 'Only these file types are allowed ( JPG | PNG  )';
        return false;
      }
      this.file = fileInput.target.files[0];
    }
  }

  removeImage() {
    this.oldContent.image = null;
    this.addForm.patchValue({pic: ''});
  }


}
