import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {AdministrationComponent} from './administration.component';
import {GenericModule} from '../generic.module';
import {FileService} from '../services/file.services';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: AdministrationComponent
      },
    ]),
    GenericModule
  ],
  providers: [FileService],
  declarations: [AdministrationComponent]
})
export class AdministrationModule {
}
