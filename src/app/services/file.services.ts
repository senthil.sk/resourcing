import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {finalize} from 'rxjs/operators';
import {AngularFireDatabase} from '@angular/fire/database';
import {AngularFireStorage} from '@angular/fire/storage';
import {Observable} from 'rxjs';

export class FileUpload {
  key: string;
  name: string;
  url: string;
  file: File;

  constructor(file: File) {
    this.file = file;
  }
}

@Injectable()
export class FileService {
  private basePath = '/uploads';
  fileUrl = null;

  constructor(private storage: AngularFireStorage, private db: AngularFirestore) {
  }

  pushFileToStorage(fileUpload: FileUpload, path, obj, variable, key, redirectBack): Observable<any> {
    const filePath = `${this.basePath}/${fileUpload.file.name}`;
    const storageRef = this.storage.ref(filePath);
    const uploadTask = this.storage.upload(filePath, fileUpload.file);

    uploadTask.snapshotChanges().pipe(
      finalize(() => {
        storageRef.getDownloadURL().subscribe(downloadURL => {
          console.log('File available at', downloadURL);
          fileUpload.url = downloadURL;
          fileUpload.name = fileUpload.file.name;
          this.fileUrl = downloadURL;
          obj[variable] = downloadURL;
          if (key !== null && key !== undefined) {
            this.updateObject(path, key, obj, redirectBack);
          } else {
            this.saveFileData(path, obj, redirectBack);
          }
        });
      })
    ).subscribe();

    return this.fileUrl;
  }

  private saveFileData(path, obj, redirectBack) {
    this.db.collection(path).add(obj);
    this.goBack(redirectBack);
  }

  updateObject(path, key, document, redirectBack) {
    this.db.collection(path).doc(key).set(document, redirectBack);
    this.goBack(redirectBack);
  }

  goBack(redirectBack = false) {
    if (redirectBack) {
      window.history.back();
    }
  }

}
