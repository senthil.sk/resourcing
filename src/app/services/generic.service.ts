import {Injectable} from '@angular/core';
import {AngularFirestore, Query} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class GenericService {

  constructor(private db: AngularFirestore) {
  }

  createObject(path, obj) {
    this.db.doc(path).set(obj);
  }

  getObject(path, key) {
    return this.db.collection(path).doc(key);
  }

  updateObject(path, key, document) {
    this.db.collection(path).doc(key).set(document);
  }

  updateSingelKeyInObject(key) {
    this.db.collection('items').doc(key).update({name: 'grokonez.com'});
  }

  deleteObject(path, key) {
    this.db.collection(path).doc(key).delete();
  }

  addToList(path, obj) {
    this.db.collection(path).add(obj);
  }

  getListOfObjects(path) {
    return this.db.collection(path, ref => ref.orderBy('modifiedDate', 'desc'));
  }

  getListOfObjectsByKey(path, value) {
    return this.db.collection(path, ref => ref.where('profileId', '==', value));
  }

  getListOfObjectsWithLimit(path) {
    return this.db.collection(path, ref => ref.orderBy('modifiedDate', 'desc').limit(10));
  }

  getListOfObjectsAfterLastResponse(path, lastResponse) {
    return this.db.collection(path, ref => ref.orderBy('modifiedDate', 'desc').orderBy('addedDate').limit(10).startAfter(lastResponse));
  }

  getListOfObjectsForSearchQuery(path, searchQueryList, lastResponse) {
    return this.db.collection(path, ref => {
      let query: Query = ref;
      query = query.orderBy('addedDate');
      for (const key of Object.keys(searchQueryList)) {
        if (searchQueryList[key] !== '' && searchQueryList[key] !== null) {
          if (searchQueryList[key] === 'name') {
            query = query.where(key, 'array-contains-any', searchQueryList[key]);
          } else {
            query = query.where(key, '==', searchQueryList[key]);
          }
        }
      }
      if (lastResponse !== null) {
        return query.limit(10).startAfter(lastResponse);
      } else {
        return query.limit(10);
      }
    });
  }
}
