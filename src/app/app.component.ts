import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {environment} from '../environments/environment.prod';

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  loggedIn = false;
  activeProfile = 'home';
  isAdmin = false;

  constructor(private router: Router) {
    const userDetails = sessionStorage.getItem('userDetails');
    if (userDetails === undefined || userDetails === null) {
      this.router.navigate(['/login']);
    }

    setInterval(() => {
      const email = sessionStorage.getItem('email');
      if (email !== null && email !== undefined && email !== '') {
        this.loggedIn = true;
        const admins = environment.appDetails.admin.split(',');
        for (const a of admins) {
          if (a === email) {
            this.isAdmin = true;
          }
        }
      }
    }, 10);
  }

  logout() {
    sessionStorage.clear();
    this.openLink('/login', null);
  }

  openLink(page, active) {
    this.activeProfile = active;
    setTimeout(() => {
      this.router.navigate([page]);
    }, 50);
  }
}
