//import modules installed at the previous step. We need them to run Node.js server and send emails
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const nodemailer = require("nodemailer");
const path = require('path');

// create a new Express application instance
const app = express();

//configure the Express middleware to accept CORS requests and parse request body into JSON
app.use(cors({origin: "*"}));
app.use(bodyParser.json());

// app.use(express.static(process.cwd()+"/resourcing/public/"));

// Allowed extensions list can be extended depending on your own needs
const allowedExt = [
  '.js',
  '.ico',
  '.css',
  '.png',
  '.jpg',
  '.woff2',
  '.woff',
  '.ttf',
  '.svg',
];

// Redirect all the other resquests
app.get('*', (req, res) => {
  if (allowedExt.filter(ext => req.url.indexOf(ext) > 0).length > 0) {
    res.sendFile(path.resolve(`public/${req.url}`));
  } else {
    res.sendFile(path.resolve('public/index.html'));
  }
});

//start application server on port 3000
app.listen(80, () => {
  console.log("The server started on port 80");
});

// define a sendmail endpoint, which will send emails and response with the corresponding status
app.post("/api/sendmail", (req, res) => {
  console.log("request came");
  let user = req.body;
  sendMail(user, (err, info) => {
    if (err) {
      console.log(err);
      res.status(400);
      res.send({error: "Failed to send email"});
    } else {
      console.log("Email has been sent");
      res.send(info);
    }
  });
});

const sendMail = (user, callback) => {
  const mailOptions = {
    from: `"Senthil", "senthilk979@gmail.com"`,
    to: user.toMail,
    cc: user.ccMail,
    subject: user.subject,
    html: user.html
  };
  if (user.resume !== null && user.resume !== undefined) {
    mailOptions.attachments = [{
      filename: user.fileName,
      path: user.resume
    }]
  }
  const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 587,
    secure: false,
    auth: {
      user: "senthilk979@gmail.com",
      pass: "xbwqajasdgocqatf"
    }
  });
  transporter.sendMail(mailOptions, callback);
}
